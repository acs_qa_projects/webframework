   # WebFramework
<b>`Si vous avez une application sur les platformes web et voulez tout automatiser avec un seul code de base? Ce framework est pour vous.`</b><br><br>
Ceci est un modèle générique de Page Object qui resout tous vos besoins d'automatisation avec un seul code.<br>
Nous avons tendance à créer différents frameworks de tests pour différentes plateformes et c'est très difficile pour toute personne de servir tous les besoins des plateformes dans un seul framework de test d'automation.<br>

<b>`WebFramework resous tous vos besoins. Vous ajoutez juste vos locators et laissez le reste à WebFramework.`</b><br>

## Contenu:

* [Fonctionnalités](#Fonctionnalités)
* [Librairies Utilisées](#librairies-Utilisées)
* [Prérequis d'Installations](#prérequis-d'installations)
* [Comment ça marche](#Comment ça marche)
* [Comment exécuter vos tests](#Comment exécuter vos tests)
* [Comment voir les résultats de rapports Spark](#Comment voir les résultats de rapports Spark)

## Fonctionnalités:

* Facile à automatiser tout type d'application web
* Execution parallel de tests
* BDD avec Cucumber
* Page Object Model
* Spark Reporting
* Robuste par nature

## Librairies Utilisées:

1. Selenium WebDriver
2. BDD Cucumber
3. Java
4. Maven
5. WebDriverManager
6. Spark Report

## Prérequis d'Installations:

1. <b>`JAVA 11`</b> - Installer Java and fixer le chemin de JAVA sur votre machine.
2. <b>`Maven`</b> - Installer Maven.

## Comment ça marche:
Ce framework est construit sur le modèle Page Object utilisant Cucumber BDD framework.<br>
Nous avons le fichier TestNGTestRunner.java pour lancer les tests en parallel sur le navigateur de votre choix chrome, firefox ou edge.
Ou, vous pouvez lancer la commande 'mvn test' dans votre terminal pour lancer les tests.

###### Voici un minimal de prérequis à avoir:

- Créer vos tests en créant des features et step definitions
- Créer vos classes Page Object de test que vous avez écrit, si pas encore déjà créer (Prenez la référence du framework <b>GreenCart.pageObjects</b>)
- Si application web, Mettre l'URL de l'application web dans le BaseTest.

## Comment exécuter vos tests:

1. Cloner le repo.<br>
   ` `<br>


## Comment voir les rapports de tests Spark:

Une fois l'exécution des tests est terminé, les résultats spark sont générés dans le dossier 'SparkReport' dans les testOutput.

