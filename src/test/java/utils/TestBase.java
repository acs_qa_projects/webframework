package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import pageObjects.LandingPage;

public class TestBase {

	public WebDriver driver;
	
	public WebDriver WebDriverManager() throws IOException
	{
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"//src//test//resources//global.properties");
		Properties prop = new Properties();
		prop.load(fis);
		String url = prop.getProperty("QAUrl");
		//Retrieve browser parameter from json

		//Retrieve browser parameter in global.properties file
		String browser_properties = prop.getProperty("browser");
		//Retrieve browser parameter in command line
		String browser_maven=System.getProperty("browser");
		// result = testCondition ? value1 : value2

		//set priority to get browser from command line or else from global.properties file
		String browser = browser_maven!=null ? browser_maven : browser_properties;

		//initialized once for one test
		if(driver == null)
		{
			if(browser.equalsIgnoreCase("chrome"))
			{
				System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"//src//test//resources//chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--remote-allow-origins=*");
				driver = new ChromeDriver(options);// driver gets the life
			}
			if(browser.equalsIgnoreCase("firefox"))
			{
				System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"//src//test//resources//geckodriver.exe");
				driver = new FirefoxDriver();
			}
			//Doesn't run parallely Need to solve this problem
			if(browser.equalsIgnoreCase("edge"))
			{
				System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"//src//test//resources//msedgedriver.exe");
				driver = new EdgeDriver();
			}
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

		driver.get(url);
		}
		
		return driver; //Here, we get driver
		
	}
	
	
	
}

